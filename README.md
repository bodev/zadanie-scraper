## Task ##
We would like to easily measure our SEO success. On the daily basis, we want to check how many times website "www.boataround.com" occurs, when you search word "Boataround" with google search engine. Can you create automated website scrapper - Crawler, to automate this task? 

### Conditions ###
Crawler must fulfil following conditions:

* Use google search engine - www.google.sk
* Search for word "Boataround"
* Count only search results containing website url - www.boataround.com
* Do NOT count adverts
* Crawl through the first 3 google result pages and count all occurences by previous criteria
* Print final message with results "Number of Boataround links is : x"

### Technology ###
You are free to use technology, language, tools according your needs, but we need detailed instructions how to install & use entire setup. Preferred languages are Python and Node.js, but they are not required. 

### Delivery ###

Pack source code to .zip and send it to pavel@boataround.com and to miroslav.kostka@boataround.com

## Last words ##

Thank you very much for the time you spend with this test, we really appreciate it! Good luck and in case of any questions/issues, please forward e-mail to miroslav.kostka@boataround.com